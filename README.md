# Desafio Back-End Developer Jr

Este é um desafio prático para back-end developers (Jr) que desejam entrar para o nosso time.

O desafio consiste na criação de uma API REST ou GraphQL de apontamento de horas.

## Modelo de dados:

- Apontamento
    * Data
    * Hora início
    * Hora fim
    * Usuário


## Pré-requisitos

- Deve ser possível:
    * cadastrar apontamentos, 
    * editar apontamento
    * listar apontamentos
    * filtrar por apontamento de um usuário específico
    * excluir apontamento específico;
- API com REST;
- A persistência dos dados deve ser no PostgreSQL ou MongoDB;
- Obrigatório a utilização de NodeJS 12+ com ECMAScript;
- O projeto deve ser publicado em um repositório publico do Github/Bitbucket/Gitlab (crie uma se você não possuir).

## Desejável (Opcional)

-   Analise de código com ESlint
-   Melhore a qualidade do código estendendo algum style guide
-   Utilize TypeScript
-   Utilizar testes automatizados (Mocha, Chai, TDD).

## O que esperamos:

-   Utilização de boas práticas
-   Técnicas de clean code
-   Que o app suba apenas com o comando: yarn start ou npm start

## Plus (Opcional)
Você ganhará mais pontos se implementar os itens a seguir

-   Publicar a aplicação em algum serviço como Heroku, Netlify, ou similar. Todos são gratuitos para projetos pessoais.
-   Publique em um repositório docker hub via pipeline de CI
-   API com GraphQL;
-   Containerize a aplicação com docker
-   Crie os seguintes pipelines de CI (Circle CI, ou similar): 
    * Build: instalar as dependências
    * Test: utilizar o cache de dependências e rodar todos os testes
